package com.leitura.model;

import java.math.BigDecimal;

public class Item {
	
	private int id;
	private int quantidade;
	private BigDecimal preco;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	public BigDecimal getPreco() {
		return preco;
	}
	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}
	
	public BigDecimal getValorTotal() {
		return preco.multiply(new BigDecimal(quantidade));
	}

	/**
	 * Construtor do Objeto Item 
	 * @param linhaFormatada  (id-quantidade-preco)
	 */
	public Item(String linhaFormatada) {
		
		String[] propriedades = linhaFormatada.split("-");
		
		this.id = Integer.parseInt(propriedades[0]);
		this.quantidade = Integer.parseInt(propriedades[1]);
		this.preco = new BigDecimal(propriedades[2]);
		
		
		
	}
	
	
}
