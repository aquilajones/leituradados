package com.leitura.model;

import java.math.BigDecimal;

public class Vendedor {
	private String nome;
	private String cpf;
	private BigDecimal salario;
	private BigDecimal acumuladorValorVenda = BigDecimal.ZERO;

	/**
	 * Construtor do Objeto Vendedor
	 * @param linhaFormatada (001�CPF�Name�Salar)
	 */
	public Vendedor(String linhaFormatada) {
		String[] propriedades = linhaFormatada.split("\\W.");
		this.cpf = propriedades[1];
		this.nome = propriedades[2];
		this.salario = new BigDecimal(propriedades[3]);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public BigDecimal getSalario() {
		return salario;
	}

	public void setSalario(BigDecimal salario) {
		this.salario = salario;
	}
	public BigDecimal getAcumuladorValorVenda() {
		return acumuladorValorVenda;
	}

	public void setAcumuladorValorVenda(BigDecimal acumuladorValorVenda) {
		this.acumuladorValorVenda = acumuladorValorVenda;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((salario == null) ? 0 : salario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vendedor other = (Vendedor) obj;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		
		return true;
	}

	@Override
	public String toString() {
		return "[nome=" + nome + ", cpf=" + cpf + "]";
	}





	
}
