package com.leitura.model;

import java.math.BigDecimal;
import java.util.List;

public class Venda {

	private int id;
	private List<Item> listaItem ;
	private Vendedor vendedor;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<Item> getListaItem() {
		return listaItem;
	}
	public void setListaItem(List<Item> listaItem) {
		this.listaItem = listaItem;
	}
	public Vendedor getVendedor() {
		return vendedor;
	}
	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}
	
	public BigDecimal getValorTotal() {
		BigDecimal retorno  = BigDecimal.ZERO;
		
		for (Item item : listaItem) {
			retorno = retorno.add(item.getValorTotal());
		}
		
		return retorno;
	}
	@Override
	public String toString() {
		return "[Id=" + id + ", Total = " + getValorTotal() + " ]";
	}
	
	
}
	
