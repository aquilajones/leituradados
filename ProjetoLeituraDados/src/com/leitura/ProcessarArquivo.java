package com.leitura;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.leitura.model.Item;
import com.leitura.model.Venda;
import com.leitura.model.Vendedor;
import com.leitura.util.ArquivoUtil;

public class ProcessarArquivo implements Runnable {
	private static final String PREFIXO_VENDA = "003";
	private static final String PREFIXO_VENDEDOR = "001";
	private static final String PREFIXO_CLIENTE = "002";
	private static final String DIR_INPUT =  "data" + File.separator + "in";
	private static final String DIR_OUTPUT =  "data" + File.separator + "out";
	private static final String DIR_ERRO = "data" + File.separator + "error";
	private static final String DIR_PROCESSADO = "data" + File.separator + "processado";
	private static final String EXTENSAO_DAT = ".dat";
	private static  String DIR_PARDAO = System.getProperty("user.dir") +File.separator ;
	@Override
	public void run() {
		File file = null;
		if (System.getenv("HOMEPATH") != null && !System.getenv("HOMEPATH").toString().isEmpty()) {
			DIR_PARDAO =System.getenv("HOMEPATH") + File.separator;
		}
		File listFiles[] = ArquivoUtil.verificarArquivosEntrada(DIR_PARDAO+DIR_INPUT,EXTENSAO_DAT);
		for (int i = 0; i < listFiles.length; i++) {
			try {
				file = listFiles[i];
				List<Venda> listaVenda = new ArrayList<>();
				List<Vendedor> listaVendendor = new ArrayList<>();
				String[] linhas = ArquivoUtil.leArquivo(file);
				int countClient = 0;
				for (String linha : linhas) {

					String prefixo = linha.substring(0, 3);

					switch (prefixo) {
					case PREFIXO_CLIENTE:
						countClient++;
						break;
					case PREFIXO_VENDEDOR:
						Vendedor vendendor = new Vendedor(linha);
						if (!listaVendendor.contains(vendendor)) {
							listaVendendor.add(vendendor);
						}
						break;
					case PREFIXO_VENDA:
						int aux1 = linha.indexOf("[");
						int aux2 = linha.indexOf("]");
						String[] linhaSplit = null;
						Venda venda = new Venda();
						venda.setVendedor(
								listaVendendor.stream().filter(p -> linha.contains(p.getNome())).findFirst().get());
						String itens = linha.substring(aux1 + 1, aux2);
						venda.setListaItem(criarItens(itens));
						linhaSplit = linha.split("\\W.");
						venda.setId(Integer.parseInt(linhaSplit[1]));
						listaVenda.add(venda);
						break;
					default:
						break;
					}

				}

				listaVenda.sort(Comparator.comparing(Venda::getValorTotal));
				obtemQuantidadeVendaVendedor(listaVenda, listaVendendor);
				StringBuilder texto = montarTextoSaida(listaVenda, listaVendendor, countClient);
				ArquivoUtil.salvarArquivoSaida(texto.toString(), file.getName().replace(EXTENSAO_DAT, ".done.dat"), DIR_PARDAO+DIR_OUTPUT);
				ArquivoUtil.moveArquivo(file, DIR_PARDAO+DIR_PROCESSADO);
			} catch (Exception e) {
				if (file != null) {
					ArquivoUtil.moveArquivo(file, DIR_PARDAO + DIR_ERRO);
				}
			}
		}
	}



	private static StringBuilder montarTextoSaida(List<Venda> listaVenda, List<Vendedor> listaVendendor,
			int countClient) {
		StringBuilder texto = new StringBuilder("Relatorio SAIDA").append("\n");
		texto.append("\n");
		texto.append("Quantidade de Clientes :");
		texto.append(countClient).append("\n");
		texto.append("Quantidade de Vendedores :");
		texto.append(listaVendendor.size()).append("\n");		
		texto.append("Venda de Maior Valor : ");
		texto.append(listaVenda.get(0).toString()).append("\n");
		texto.append("Vendedor com menor valor de venda acumulado : ");
		texto.append(listaVendendor.get(listaVendendor.size()-1).toString());
		return texto;
	}



	private static void obtemQuantidadeVendaVendedor(List<Venda> listaVenda, List<Vendedor> listaVendendor) {

		for (Venda venda : listaVenda) {

			listaVendendor.forEach((p) -> {
				if (p.equals(venda.getVendedor())) {
					p.setAcumuladorValorVenda(p.getAcumuladorValorVenda().add(venda.getValorTotal()));
				}

			});

		}
		
		listaVendendor.sort(Comparator.comparing(Vendedor::getAcumuladorValorVenda));
		
	}



	private static List<Item> criarItens(String itens) {
		List<Item> retorno = new ArrayList<>();
		String[] sIten = itens.split(",");

		for (int i = 0; i < sIten.length; i++) {
			Item it = new Item(sIten[i]);
			retorno.add(it);
		}

		return retorno;
	}




	public boolean isFinalizado() {
		// TODO Auto-generated method stub
		return false;
	}



}
