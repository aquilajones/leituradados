package com.leitura;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RunLeitura {

	public static void main(String[] args) {
		try {
			ProcessarArquivo leitura = new ProcessarArquivo();
			ExecutorService threadExecutor = Executors.newFixedThreadPool(1);
			while (!leitura.isFinalizado()) {
				threadExecutor.execute(leitura);
				Thread.sleep(1000);
			}
			threadExecutor.shutdown();
		} catch (Exception e) {
			System.exit(0);
		}

	}
}
