package com.leitura.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;

public final class  ArquivoUtil {

	public static String[] leArquivo(File file) throws FileNotFoundException, IOException {
		String linhas[] = null;
		if (file.exists()) {
			FileReader reader = new FileReader(file);
			BufferedReader buffReader = new BufferedReader(reader);
			String linha = "";
			Vector vecLinhas = new Vector();
			while (linha != null) {
				linha = buffReader.readLine();
				if (linha != null) {
					vecLinhas.add(linha);
				}
			}
			buffReader.close();
			linhas = new String[vecLinhas.size()];
			linhas = (String[]) vecLinhas.toArray(linhas);
		} else {
		}
		return linhas;
	}

	public static File[] verificarArquivosEntrada(String dir,String extensao) {

		File diretorio = new File(dir);

		if (!diretorio.exists()) {
			diretorio.mkdirs();
		}

		return diretorio.listFiles((p) -> p.isFile() && p.getName().contains(extensao));
	}

	public static void salvarArquivoSaida(String texto, String nomeSaida, String dirFinal) throws Exception {

		File diretorio = new File( dirFinal);

		if (!diretorio.exists()) {
			diretorio.mkdirs();
		}
		FileWriter arquivo = new FileWriter(new File(diretorio + File.separator + nomeSaida));
		arquivo.write(texto.toString());
		arquivo.close();
	}

	
	
	public static void moveArquivo(File file , String dirFinal) {
		
	    InputStream in;
	    OutputStream out;
	    try{
	        File fromFile = new File(dirFinal+File.separator+file.getName() );
	        if(!fromFile.exists()){
	            if(!fromFile.getParentFile().exists()){
	                fromFile.getParentFile().mkdirs();
	            }
	            fromFile.createNewFile();
	        }
	        in = new FileInputStream(file);
	        out = new FileOutputStream(fromFile);
	        byte[] buffer = new byte[1024];
	        int length;
	        while((length = in.read(buffer)) > 0 ){
	            out.write(buffer, 0 , length);
	        }

	        in.close();
	        out.close();
	        file.delete();

	    }catch(IOException e){
	        e.printStackTrace();
	    }
		
		
	}

}
